--Affichage des Emprunts des Adhérents (exemple : 'apple')--
SELECT  Film.titre
       ,Emprunt.date_pret
       ,Emprunt.date_retour
FROM Emprunt
JOIN Exemplaire
ON Emprunt.exemplaire = Exemplaire.id
JOIN Film
ON Film.code = Exemplaire.code_film
WHERE Emprunt.adherent = 'apple';

SELECT  OeuvreMusicale.titre
       ,Emprunt.date_pret
       ,Emprunt.date_retour
FROM Emprunt
JOIN Exemplaire
ON Emprunt.exemplaire = Exemplaire.id
JOIN OeuvreMusicale
ON OeuvreMusicale.code = Exemplaire.code_oeuvre
WHERE Emprunt.adherent = 'apple';

SELECT  Livre.titre
       ,Emprunt.date_pret
       ,Emprunt.date_retour
FROM Emprunt
JOIN Exemplaire
ON Emprunt.exemplaire = Exemplaire.id
JOIN Livre
ON Livre.code = Exemplaire.code_livre
WHERE Emprunt.adherent = 'apple';

--Affichage des Exemplaires de tel auteur - réalisateur - compositeur aux Adhérents--
--CHANGEMENTS JSON
SELECT  Livre.titre
       ,a->>'nom'    AS Nom
       ,a->>'prenom' AS Prénom
       ,COUNT(Exemplaire.id)  AS NbExemplaires
FROM Livre
LEFT JOIN JSON_ARRAY_ELEMENTS(Livre.auteur) a
ON TRUE
JOIN Exemplaire
ON Livre.code = Exemplaire.code_livre
WHERE a->>'nom' = 'Beyle'
AND a->>'prenom' = 'Henri'
GROUP BY  Livre.titre
         ,a->>'nom'
         ,a->>'prenom'
ORDER BY NbExemplaires ASC;

--Remarque : Erreur lorsque la requête utilise FROM Livre,JSON_ARRAY_ELEMENTS(p.adresse) puis JOIN Exemplaire
--Il faut donc considerer le JSON_ARRAY comme une jointure LEFT JOIN JSON_ARRAY_ELEMENTS(p.adresse) et ajouter ON TRUE

SELECT  Film.titre
       ,a->>'nom'    AS Nom
       ,a->>'prenom' AS Prénom
       ,COUNT(Exemplaire.id)  AS NbExemplaires
FROM Film
LEFT JOIN JSON_ARRAY_ELEMENTS(Film.acteur) a
ON TRUE
JOIN Exemplaire
ON Film.code = Exemplaire.code_film
WHERE a->>'nom' = 'Travolta'
AND a->>'prenom' = 'John'
GROUP BY  Film.titre
         ,a->>'nom'
         ,a->>'prenom'
ORDER BY NbExemplaires ASC;

SELECT  OeuvreMusicale.titre
       ,c->>'nom' AS Nom
       ,c->>'prenom' AS Prénom
       ,COUNT(Exemplaire.id)    AS NbExemplaires
FROM OeuvreMusicale
LEFT JOIN JSON_ARRAY_ELEMENTS(OeuvreMusicale.compositeur) c
ON TRUE
JOIN Exemplaire
ON OeuvreMusicale.code = Exemplaire.code_oeuvre
WHERE c->>'nom' = 'Maalouf'
AND c->>'prenom' = 'Ibrahim'
GROUP BY  OeuvreMusicale.titre
         ,c->>'nom'
         ,c->>'prenom'
ORDER BY NbExemplaires ASC;

--Affichage de tous les contributeurs des Films, des Livres et des OeuvreMusicale
SELECT Film.titre, 
       a->>'nom' AS nomActeur, 
       a->>'prenom' AS prénomActeur,
       r->>'nom' AS nomRéalisateur, 
       r->>'prenom' AS prénomRéalisateur
FROM Film, 
       JSON_ARRAY_ELEMENTS(Film.realisateur) r,
       JSON_ARRAY_ELEMENTS(Film.acteur) a;

--realisateurs des films
SELECT Film.titre, 
       r->>'nom' AS nomRéalisateur, 
       r->>'prenom' AS prénomRéalisateur
FROM Film, 
       JSON_ARRAY_ELEMENTS(Film.realisateur) r;

--acteurs des films
SELECT Film.titre, 
       a->>'nom' AS nomActeur, 
       a->>'prenom' AS prénomActeur
FROM Film, 
       JSON_ARRAY_ELEMENTS(Film.acteur) a;

SELECT OeuvreMusicale.titre, 
       c->>'nom' AS nomCompositeur, 
       c->>'prenom' AS prénomCompositeur, 
       i->>'nom' AS nomInterprète, 
       i->>'prenom' AS prénomInterprète
FROM OeuvreMusicale, 
       JSON_ARRAY_ELEMENTS(OeuvreMusicale.compositeur) c, 
       JSON_ARRAY_ELEMENTS(OeuvreMusicale.interprete) i;

SELECT Livre.titre, 
       a->>'nom' AS nomAuteur, 
       a->>'prenom' AS prénomAuteur
FROM Livre, 
       JSON_ARRAY_ELEMENTS(Livre.auteur) a;

--Affichage des Livres - Films - Oeuvres musicales aux Adhérents--
SELECT  *
FROM Livre;

SELECT  *
FROM Film;

SELECT  *
FROM OeuvreMusicale;

--Modifier leur description-- UPDATE Film

SET synopsis = {}
WHERE code = {} UPDATE Livre

SET resume = {}
WHERE code = {}

--Ajouter des exemplaires d'une Ressource--
INSERT INTO Exemplaire VALUES (etat,disponible,code_oeuvre,code_film,code_livre);

--Gestion des Prêts par un MembrePersonnel--
SELECT  Film.titre
       ,Emprunt.date_pret
       ,Emprunt.date_retour
FROM Emprunt
JOIN Exemplaire
ON Emprunt.exemplaire = Exemplaire.id
JOIN Film
ON Film.code = Exemplaire.code_film
WHERE Emprunt.personnel = 'mazir';
--Gestion des sanctions par MembrePersonnel--
SELECT  Livre.titre
       ,Emprunt.date_pret
       ,Emprunt.date_retour
       ,Emprunt.adherent
       ,Retard.personnel
FROM Retard
JOIN Emprunt
ON Emprunt.retard = Retard.id
JOIN Exemplaire
ON Emprunt.exemplaire = Exemplaire.id
JOIN Livre
ON Livre.code = Exemplaire.code_livre;

SELECT  Film.titre
       ,Emprunt.date_pret
       ,Emprunt.date_retour
       ,Emprunt.adherent
       ,Deterioration.personnel
FROM Deterioration
JOIN Emprunt
ON Emprunt.deterioration = Deterioration.id
JOIN Exemplaire
ON Emprunt.exemplaire = Exemplaire.id
JOIN Film
ON Film.code = Exemplaire.code_film;

SELECT  Adherent.nom
       ,Adherent.prenom
       ,Adherent.actif
       ,Deterioration.id
FROM Adherent
LEFT JOIN Emprunt
ON Emprunt.adherent = Adherent.login
LEFT JOIN Deterioration
ON Deterioration.id = Emprunt.deterioration;

SELECT  Adherent.nom
       ,Adherent.prenom
       ,Adherent.actif
       ,Retard.id
FROM Adherent
LEFT JOIN Emprunt
ON Emprunt.adherent = Adherent.login
LEFT JOIN Retard
ON Retard.id = Emprunt.retard;

--Affichage des données des utilisateurs pour un Adhérent--
SELECT  nom
       ,prenom
       ,date_naissance
       ,code_postal
       ,adresse_rue
       ,ville
       ,adresse_mail
       ,num_tel
       ,actif
       ,droit_emprunt
FROM Adherent
WHERE login = 'apple';

--Les genres populaires pour un adhérent (exemple d'apple Film)--

CREATE VIEW PopularitéFilmApple AS
SELECT  Film.titre
       ,Film.genre
       ,COUNT(*) AS Popularité
FROM Emprunt
JOIN Exemplaire
ON Exemplaire.id = Emprunt.exemplaire
JOIN Film
ON Exemplaire.code_film = Film.code
WHERE Emprunt.adherent = 'apple'
GROUP BY  genre
         ,titre
ORDER BY Popularité ASC;

--Profil des Adhérents (exemple d'apple pour Film)--
SELECT  Film.titre
FROM Film
LEFT JOIN PopularitéFilmApple
ON Film.titre = PopularitéFilmApple.titre
WHERE PopularitéFilmApple.titre IS NULL
AND Film.genre = 'gangsters'
GROUP BY  Film.titre;
--'gangsters' sera récupéré avec le code Python

--Les exemplaires populaires--

CREATE VIEW PopularitéFilm AS
SELECT  Film.titre
       ,Film.genre
       ,COUNT(*) AS Popularité
FROM Emprunt
JOIN Exemplaire
ON Exemplaire.id = Emprunt.exemplaire
JOIN Film
ON Exemplaire.code_film = Film.code
GROUP BY  titre
         ,genre
ORDER BY Popularité ASC;

CREATE VIEW PopularitéOeuvre AS
SELECT  OeuvreMusicale.titre
       ,OeuvreMusicale.genre
       ,COUNT(*) AS Popularité
FROM Emprunt
JOIN Exemplaire
ON Exemplaire.id = Emprunt.exemplaire
JOIN OeuvreMusicale
ON Exemplaire.code_oeuvre = OeuvreMusicale.code
GROUP BY  titre
         ,genre
ORDER BY Popularité ASC;

CREATE VIEW PopularitéLivre AS
SELECT  Livre.titre
       ,Livre.genre
       ,COUNT(*) AS Popularité
FROM Emprunt
JOIN Exemplaire
ON Exemplaire.id = Emprunt.exemplaire
JOIN Livre
ON Exemplaire.code_livre = Livre.code
GROUP BY  titre
         ,genre
ORDER BY Popularité ASC;

--Affichage des genres--
SELECT  *
FROM Livre
WHERE genre = {};

SELECT  *
FROM Film
WHERE genre = {};

SELECT  *
FROM OeuvreMusicale
WHERE genre = {};
--Update des Retards (avec comparaison avec DateDuJour)--

CREATE VIEW FinRetard AS
SELECT  Adherent.login                                                  AS Adhérent
       ,Emprunt.exemplaire                                              AS Exemplaire
       ,Emprunt.retard                                                  AS Retard
       ,Emprunt.date_rendu + (Emprunt.date_rendu - Emprunt.date_retour) AS DateDeFin
       ,DATE(NOW())                                                     AS DateDuJour
FROM Emprunt
JOIN Adherent
ON Emprunt.adherent = Adherent.login
WHERE Emprunt.retard = 1; 

UPDATE Retard
SET fin = '1'
WHERE Retard.id = 1; 

UPDATE Adherent
SET droit_emprunt = 'True'
WHERE Adherent.login = 'otari'; 
